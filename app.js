const express = require( 'express' )
const hbs = require( 'hbs' );

require( 'dotenv' ).config()

const app = express()
const port = process.env.PORT;

//Handlebars
app.set( 'view engine', 'hbs' );
hbs.registerPartials( __dirname + '/views/shared' );

// serve static content
app.use( express.static( 'public' ) );

app.get( '/', ( req, res )  => {
    res.render( 'home', {
        name: 'Urel Lozano',
        title: 'Course Node'
    } );
})

app.get( '/generic', ( req, res )  => {
    res.render( 'generic', {
        name: 'Urel Lozano',
        title: 'Course Node'
    } );
})

app.get( '/elements', ( req, res )  => {
    res.render( 'elements', {
        name: 'Urel Lozano',
        title: 'Course Node'
    } );
})

app.get('*', ( req, res ) => {
    res.sendFile( __dirname + '/public/404.html' );
})
  
app.listen( port, () => {
    console.log( `Example app listening on port ${port}` )
} )